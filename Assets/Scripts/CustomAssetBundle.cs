﻿using System;
using System.Linq;
using UnityEngine;

namespace Manager.Scripts.Utility
{
    public class CustomAssetBundle : MonoBehaviour
    {
        private static CustomAssetBundle _instance;

        private void Awake()
        {
            _instance = this;
        }

        public static CustomAssetBundle GetInstance()
        {
            return _instance;
        }

        #region prefabs

        [Serializable]
        public class Prefab
        {
            public string name;
            public GameObject prefabGameObject;
        }

        public Prefab[] prefabs;

        public GameObject GetPrefab(string name)
        {
            return (
                from prefab in prefabs
                where string.Equals(prefab.name, name, StringComparison.CurrentCultureIgnoreCase)
                select prefab.prefabGameObject).FirstOrDefault();
        }

        #endregion


        #region sprites

        [Serializable]
        public class Spritess
        {
            public string name;
            public Sprite SpriteObject;
        }

        public Spritess[] sprites;

        public Sprite GetSprite(string name)
        {
            foreach (var sprite in sprites)
                if (sprite.name.ToUpper() == name.ToUpper())
                    return sprite.SpriteObject;

            return null;
        }

        #endregion
    }
}