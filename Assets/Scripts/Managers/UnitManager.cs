﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Unit;
using Manager.Scripts.Tile;
using Manager.Scripts.Unit;
using Manager.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    class UnitManager : MonoBehaviour
    {
        public UnitUpgrades CurrentUpgrades;
        public List<Unit> _Units = new List<Unit>();
        private int MaxCount = 10;

        private float SpawnTimerRemaning = 0;
        public float SpawnTimer = 20;


        private void Awake()
        {
            _instance = this;
            CurrentUpgrades = UnitUpgrades.CreateFromJSON(UpgradesTextAsset.text);
        }

        public Unit GetClosestUnitTo(Vector3 pos)
        {
            Unit CurrentClosest = null;
            float currentDistance = float.MaxValue;

            foreach (var unit in _Units)
            {
                float distance = Math.Abs(pos.x - unit.transform.position.x) +
                                 Math.Abs(pos.y - unit.transform.position.y);

                if (distance < currentDistance)
                {
                    currentDistance = distance;
                    CurrentClosest = unit;
                }
            }

            return CurrentClosest;
        }

        public TextAsset UpgradesTextAsset;
        private void Start()
        {

        }

        public void SetUnitMaxCount(int newCount)
        {
            if(MaxCount == newCount) return;

            SpawnTimerRemaning = SpawnTimer;
            MaxCount = newCount;
            
        }

        public string GetEquipmentStatus()
        {
            return $@"Current Weapon: {CurrentUpgrades.SwordName}
	Current Speed: {CurrentUpgrades.SwordSpeed}
	Current Damage: {CurrentUpgrades.SwordDamage}

Current Gear: {CurrentUpgrades.ArmorName}
	Current Health: {CurrentUpgrades.ArmorHealth}

Current Pickaxe: {CurrentUpgrades.PickaxeName}
	Current Speed: {CurrentUpgrades.PickaxeSpeed}
	Current Damage: {CurrentUpgrades.PickaxeDamage}

Current Inventory: {CurrentUpgrades.InventoryName}
    Slots: {CurrentUpgrades.InventorySlots}";

        }

        public string GetUnitsStatus()
        {
            var returnString = $"Units: {_Units.Count}/{MaxCount}.";
            
            if(_Units.Count < MaxCount)
                returnString += $" Next Spawn in {SpawnTimerRemaning:0.0}";

            return returnString;
        }

        public bool PrintNewUpgradesJSON = false;
        public void Update()
        {
            if (PrintNewUpgradesJSON)
            {
                CurrentUpgrades.WriteJSON();
                PrintNewUpgradesJSON = false;
            }

            if (_Units.Count == 0)
            {
                TileManager.GetInstance().PreviousLayer();
                LogManager.GetInstance().AddLog("All units dead, moving back one floor");
                return;
            }
                


            if (MaxCount <= _Units.Count)
                return;

            SpawnTimerRemaning -= Time.deltaTime;

            if (SpawnTimerRemaning > 0)
                return;

            SpawnTimerRemaning = SpawnTimer;
            DirectlySpawnUnit(1);

        }

        public int ResetMap()
        {
            var prevUnitCount = _Units.Count;
            for (int i = 0; i < _Units.Count; i++)
            {
                Destroy(_Units[i].gameObject);
            }
            _Units.Clear();

            //DirectlySpawnUnit(prevUnitCount);
            return prevUnitCount;
        }

        public void DirectlySpawnUnit(int amount)
        {
            for (var i = 0; i < amount; i++)
            {
                var unit = Instantiate(CustomAssetBundle.GetInstance().GetPrefab("unit"), TileManager.GetInstance().HomePos + new Vector3(0.5f,0.5f), Quaternion.identity).GetComponent<Unit>();
                unit.gameObject.transform.SetParent(TileManager.GetInstance().Units,true);
                _Units.Add(unit);
            }
        }

        private static UnitManager _instance;
        public static UnitManager GetInstance()
        {
            return _instance;
        }

    }
}
