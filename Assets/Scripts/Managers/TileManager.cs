﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Manager.Scripts.Utility;
using Pathfinding;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Manager.Scripts.Tile
{
    //[ExecuteInEditMode]
    public class TileManager : MonoBehaviour
    {
        public int RemaningMinable = 0;
        [System.Serializable]
        public class LevelTileSpawn
        {
            public TileSpawnChange[] TileSpawnChanges;

            [Header("Enemies")]
            public int EnemyHealthMin = 0;
            public int EnemyHealthMax = 0;

            public int EnemyDamageMin = 0;
            public int EnemyDamageMax = 0;

            [System.Serializable]
            public class TileSpawnChange
            {
                public BaseTile.TileType Tiletype;
                public int Chance;
            }
        }

        private bool MapIsDone = false;
        
        

        public LevelTileSpawn[] LevelTileSpawns;
        public int CurrentLayer = 0;

        //Setting up the grid
        public int maxX = 20;
        public int maxY = 1;

        public Transform Units;
        public Transform Enemies;

        public AstarPath _AstarPath;

        public Vector3Int HomePos;

        public Tilemap _Grid;

        public int RandomSeed = 12;

        private readonly List<Vector3Int> _availBaseTiles = new List<Vector3Int>();
        private readonly List<Vector3Int> _ReservedBaseTiles = new List<Vector3Int>();
        private readonly List<Vector3Int> UpdatePathArea = new List<Vector3Int>();

        [Header("UIText")]
        public Text RemaningText = null;
        public Text FloorText = null;
        public Text MapText = null;
        public int HighestReachedFloor = 0;
        public int HighestReachedFloor_MapsCompleated = 0;
        public Text NextFloorButton = null;

        [Header("TileStartingHealth")]
        public float DirtHealth = 0;
        public float RockHealth = 0;
        public float TinHealth = 0;
        public float CopperHealth = 0;
        public float IronHealth = 0;
        public float SilverHealth = 0;
        public float GoldHealth = 0;
        public float PlatinumHealth = 0;
        public float ObsidianHealth = 0;
        public float DiamondHealth = 0;


        public void ResetMap()
        {
            var prevCount = UnitManager.GetInstance().ResetMap();
            EnemyManager.GetInstance().Reset();
            StaticSpawnMethod();
            SpawnHome();
            UnitManager.GetInstance().DirectlySpawnUnit(1);

            if (RemaningMinable < 100)
                MapIsDone = true;

            if (MapIsDone && CurrentLayer == HighestReachedFloor)
                HighestReachedFloor_MapsCompleated++;

            MapIsDone = false;
            UpdateMapAndFloorText();
        }

        private void UpdateMapAndFloorText()
        {
            FloorText.text = $"Floor {HighestReachedFloor}/10";

            if (CurrentLayer == HighestReachedFloor)
                MapText.text = $"Map {HighestReachedFloor_MapsCompleated}/10";
            else
                MapText.text = $"Map 10/10";

            NextFloorButton.text = HighestReachedFloor_MapsCompleated >= 10 ? "Next Floor" : "--Locked--";
        }

        public void NextLayer()
        {
            if (CurrentLayer == HighestReachedFloor)
            {
                if(HighestReachedFloor_MapsCompleated < 10)
                    return;
                HighestReachedFloor_MapsCompleated = 0;
            }

            if (CurrentLayer + 1 >= LevelTileSpawns.Length)
                return;
            
            CurrentLayer++;
            ResetMap();
            UpdateMapAndFloorText();
        }

        public void PreviousLayer()
        {
            if (CurrentLayer - 1 < 0)
                return;


            CurrentLayer--;
            ResetMap();
            UpdateMapAndFloorText();
        }


        // Use this for initialization
        private void Awake()
        {

            _instance = this;
            Random.InitState(RandomSeed);
        }

        private void Start()
        {
            StaticSpawnMethod();
            SpawnHome();
            UnitManager.GetInstance().DirectlySpawnUnit(1);
            //_AstarPath.Scan();
            UpdateMapAndFloorText();
        }

        private static TileManager _instance;
        public static TileManager GetInstance()
        {
            return _instance;
        }
        public void LateUpdate()
        {
            if(RunScan)
            {
                RunScan = false;
                _AstarPath.Scan();
            }

            foreach (var vector3Int in UpdatePathArea)
            {
                var update = new GraphUpdateObject(new Bounds(vector3Int, Vector3.one * 3));
                AstarPath.active.UpdateGraphs(update);

                UnHideTiles(vector3Int);
            }
            UpdatePathArea.Clear();
        }

        public bool RunScan { get; set; }

        public bool SpawnEnemies = false;
        private GameObject HomeObject;

        #region StaticMapGenerator
        private void StaticSpawnMethod()
        {
            HomePos = new Vector3Int(Random.Range(20, maxX - 20), Random.Range(20, maxY - 20), 0);
            _Grid.ClearAllTiles();
            _availBaseTiles.Clear();
            RemaningMinable = 0;

            for (var x = 0; x < maxX; x++)
            for (var y = 0; y < maxY; y++)
            {
                var random = Random.Range(0, 100);
                BaseTile refTile = null;
                var position = new Vector3Int(x, y, 0);
                BaseTile.TileType spawnThisTile = BaseTile.TileType.Ground;

                var MaxFromHome = 5;
                if (Math.Abs(position.x - HomePos.x) < MaxFromHome && Math.Abs(position.y - HomePos.y) < MaxFromHome)
                    spawnThisTile = BaseTile.TileType.Dirt;
                else
                    foreach (var VARIABLE in LevelTileSpawns[CurrentLayer].TileSpawnChanges)
                    {
                        if (random > VARIABLE.Chance) continue;

                        spawnThisTile = VARIABLE.Tiletype;
                        break;
                    }

                if (spawnThisTile != BaseTile.TileType.Ground)
                    RemaningMinable++;

                refTile = SpawnTile(spawnThisTile, position);

                refTile.Coordinate = position;

                refTile.sprite = refTile.GetSprite();
                refTile.colliderType = refTile.GetColliderType();
                
                _Grid.SetTile(position, refTile);
                _Grid.SetTileFlags(position, TileFlags.None);
                
                refTile.Start();

                SetColor(position, Color.black);
            }

        }

        private BaseTile SpawnTile(BaseTile.TileType tileType, Vector3Int position)
        {
            BaseTile refTile;

            switch (tileType)
            {
                case BaseTile.TileType.Ground:
                {
                    refTile = ScriptableObject.CreateInstance<GroundTile>() as BaseTile;
                    refTile.Hidden = true;

                    if (SpawnEnemies)
                    {
                        var enemy = EnemyManager.GetInstance().DirectlySpawnEnemy(position);
                        enemy.SleepingCoordinate = position;
                        enemy.gameObject.SetActive(false);

                        enemy.Damage = Random.Range(LevelTileSpawns[CurrentLayer].EnemyDamageMin, LevelTileSpawns[CurrentLayer].EnemyDamageMax);
                        enemy.RemainingHealth = Random.Range(LevelTileSpawns[CurrentLayer].EnemyHealthMin, LevelTileSpawns[CurrentLayer].EnemyHealthMax);
                    }

                    break;
                }
                case BaseTile.TileType.Tin:
                    refTile = ScriptableObject.CreateInstance<TinTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Rock:
                    refTile = ScriptableObject.CreateInstance<RockTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Dirt:
                    refTile = ScriptableObject.CreateInstance<DirtTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Copper:
                    refTile = ScriptableObject.CreateInstance<CopperTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Iron:
                    refTile = ScriptableObject.CreateInstance<IronTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Silver:
                    refTile = ScriptableObject.CreateInstance<SilverTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Gold:
                    refTile = ScriptableObject.CreateInstance<GoldTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Platinum:
                    refTile = ScriptableObject.CreateInstance<PlatinumTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Obsidian:
                    refTile = ScriptableObject.CreateInstance<ObsidianTile>() as BaseTile;
                    break;
                case BaseTile.TileType.Diamond:
                    refTile = ScriptableObject.CreateInstance<DiamondTile>() as BaseTile;
                    break;
                default:
                    throw new ArgumentException($"TILE TYPE FINNS JU NTE IMPLEMETERAD: {tileType}");
            }

            return refTile;
        }

        private void SpawnHome()
        {
            UpdatePathArea.Clear();
            if (HomeObject != null) Destroy(HomeObject);
            HomeObject = Instantiate(CustomAssetBundle.GetInstance().GetPrefab("home"), HomePos, Quaternion.identity);
            GetTile(HomePos.x, HomePos.y)?.ReplaceWithGroundTile();

            for (int x = -3; x <= 3; x++)
            for (int y = -3; y <= 3; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }
                var baseTile = GetTile(HomePos.x + x, HomePos.y + y);
                baseTile?.ReplaceWithGroundTile();
                UpdatePathArea.Add(new Vector3Int(HomePos.x + x, HomePos.y + y,0));
            }

            //Todo: fix later so there is no clean up step
            for (var i = _availBaseTiles.Count - 1; i >= 0; i--)
            {
                if (GetTile(_availBaseTiles[i]).GetTileType() == BaseTile.TileType.Ground)
                {
                    _availBaseTiles.RemoveAt(i);
                }
            }

            RunScan = true;
        }

        #endregion

        public void AddNeighborsAsAvailable(Vector3Int coordinate)
        {
            var S = GetTile(coordinate.x - 1, coordinate.y);
            var N = GetTile(coordinate.x + 1, coordinate.y);
            var W = GetTile(coordinate.x, coordinate.y - 1);
            var E = GetTile(coordinate.x, coordinate.y + 1);

            //var SW = GetTile(coordinate.x - 1, coordinate.y - 1, coordinate.z);
            //var SE = GetTile(coordinate.x - 1, coordinate.y + 1, coordinate.z);
            //var NW = GetTile(coordinate.x + 1, coordinate.y - 1, coordinate.z);
            //var NE = GetTile(coordinate.x + 1, coordinate.y + 1, coordinate.z);

            AddTileToAvailable(S);
            AddTileToAvailable(N);
            AddTileToAvailable(W);
            AddTileToAvailable(E);
            //AddTileToAvailable(SW);
            //AddTileToAvailable(SE);
            //AddTileToAvailable(NW);
            //AddTileToAvailable(NE);
        }

        private void AddTileToAvailable(BaseTile tile)
        {
            if(tile == null || _ReservedBaseTiles.Contains(tile.Coordinate) || _availBaseTiles.Contains(tile.Coordinate))
                return;

            if (tile.Hidden)
            {
                tile.Hidden = false;
                SetColor(tile.Coordinate,tile.NeutralColor());
                EnemyManager.GetInstance().EnableMonstersAtCoordinate(tile.Coordinate);
                AddNeighborsAsAvailable(tile.Coordinate);
                return;
            }

            if(tile.GetTileType() == BaseTile.TileType.Ground)
                return;

            _availBaseTiles.Add(tile.Coordinate);
        }

        public BaseTile GetTile(int x, int y)
        {
            BaseTile retVal = null;
            try
            {

                if (x >= 0 && x < maxX && y >= 0 && y < maxY)
                    retVal = _Grid.GetTile(new Vector3Int(x, y, 0)) as BaseTile;

            }
            catch (Exception)
            {

                throw;
            }
            return retVal;
        }

        public BaseTile GetTile(Vector3Int pos)
        {
            return GetTile(pos.x, pos.y);
        }

        public void MovePosToWithingGrid(ref int x, ref int y)
        {
            if (x >= maxX) x = maxX - 1;

            if (x < 0) x = 0;

            if (y >= maxY) y = maxY - 1;

            if (y < 0) y = 0;
        }

        public BaseTile GetClosestAvailableTile2(Vector3 pos)
        {
            if (!_availBaseTiles.Any())
                return null;

            var result = from tile in _availBaseTiles
                orderby Vector3.Distance(tile, pos)
                select tile;

            if (!result.Any())
                return null;

            var returntile = GetTile(result.First());
            _availBaseTiles.Remove(returntile.Coordinate);
            _ReservedBaseTiles.Add(returntile.Coordinate);
            return returntile;
        }


        public void ReplaceTile(BaseTile oldTile, BaseTile newTile)
        {
            newTile.Coordinate = oldTile.Coordinate;
            newTile.name = oldTile.name;
            newTile.sprite = newTile.GetSprite();

            _Grid.SetTile(oldTile.Coordinate, newTile);
            _Grid.SetColliderType(newTile.Coordinate,newTile.GetColliderType());
            newTile.Start();

            _ReservedBaseTiles.Remove(newTile.Coordinate);

            if (/*oldTile.GetTileType() != BaseTile.TileType.Ground && */newTile.GetTileType() == BaseTile.TileType.Ground)
            {
                RemaningMinable--;
                RemaningText.text = $"Remaning Tiles: {RemaningMinable}";
            }

            UpdatePathArea.Add(newTile.Coordinate);
        }

        private void UnHideTiles(Vector3Int pos)
        {
            UnHideTile(pos);

            for (int x = -3; x < 3; x++)
            for (int y = -3; y < 3; y++)
                UnHideTile(pos + new Vector3Int(x,y,0));
        }

        private void UnHideTile(Vector3Int pos)
        {
            if (_Grid.GetTile(pos) is GroundTile)
                return;
            if (pos.x >= 0 && pos.x < maxX && pos.y >= 0 && pos.y < maxY)
            {
                SetColor(pos, Color.white);
            }
        }

        public void SetColor(Vector3Int coordinate, Color color)
        {
            _Grid.SetColor(coordinate, color);
        }

        public void ReturnTile(BaseTile tile)
        {
            if (tile == null || _availBaseTiles.Contains(tile.Coordinate))
                return;

           _ReservedBaseTiles.Remove(tile.Coordinate);
           _availBaseTiles.Add(tile.Coordinate);
        }
    }
}