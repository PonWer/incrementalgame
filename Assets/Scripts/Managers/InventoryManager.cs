﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Unit;
using Manager;
using Manager.Scripts.Tile;
using Manager.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public InventoryScreen InventoryScreen = null;

    public int UnitBaseCount = 5;
    public int HouseCount = 0;
    public BuyMode CurrentBuyMode = BuyMode.One;
    public int BarracsIncreasesMaxUnitCoutBy = 2;
    public Text BarrackCostText = null;

    [Header("Inventory")]
    public int StoneCount = 0;
    public int TinCount = 0;
    public int DirtCount = 0;
    public int CopperCount;
    public int DiamondCount;
    public int IronCount;
    public int PlatinumCount;
    public int GoldCount;
    public int ObsidianCount;
    public int SilverCount;



    public enum BuyMode
    {
        One,
        Ten,
        TwentyFive
    
    }

    public void ChangeBuyMode(Int32 newMode)
    {
        if (newMode == 0)
            CurrentBuyMode = BuyMode.One;
        else if (newMode == 1)
            CurrentBuyMode = BuyMode.Ten;
        else
        {
            CurrentBuyMode = BuyMode.TwentyFive;
        }
        //Debug.LogWarning($"New BuyMode={CurrentBuyMode}");
        UpdateBarrackCostText();
    }

    private void Awake()
    {
        _instance = this;
    }

    public void Start()
    {
        UnitManager.GetInstance().SetUnitMaxCount(UnitBaseCount + HouseCount * BarracsIncreasesMaxUnitCoutBy);
        UpdateBarrackCostText();
    }


    private static InventoryManager _instance;

    public static InventoryManager GetInstance()
    {
        return _instance;
    }

    public int BarrackRockCost = 50;
    public void UpdateBarrackCostText()
    {
        var BarrackCostTexttext = $"Next Barrack Cost:\r\nStones: ";
        switch (CurrentBuyMode)
        {
            case BuyMode.One:
                BarrackCostTexttext += $"{BarrackRockCost * 1}";
                break;
            case BuyMode.Ten:
                BarrackCostTexttext += $"{BarrackRockCost * 10}";
                break;
            case BuyMode.TwentyFive:
                BarrackCostTexttext += $"{BarrackRockCost * 25}";
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        BarrackCostText.text = BarrackCostTexttext;
    }

    public void ClickBuyHouseButton()
    {

        switch (CurrentBuyMode)
        {
            case BuyMode.One:
                BuyBarrack(1);
                break;
            case BuyMode.Ten:
                BuyBarrack(10);
                break;
            case BuyMode.TwentyFive:
                BuyBarrack(25);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        UnitManager.GetInstance().SetUnitMaxCount(UnitBaseCount + HouseCount * BarracsIncreasesMaxUnitCoutBy);

    }

    private void BuyBarrack(int count)
    {
        if (StoneCount < BarrackRockCost * count) return;

        StoneCount -= BarrackRockCost * count;
        InventoryManager.GetInstance().HouseCount += count;
    }

    public bool CanSpend(UnitUpgrades.UpgradeCost Cost)
    {
        if (
            Cost.Dirt <= DirtCount &&
            Cost.Stone <= StoneCount &&
            Cost.Tin <= TinCount &&
            Cost.Copper <= CopperCount &&
            Cost.Iron <= IronCount &&
            Cost.Silver <= SilverCount &&
            Cost.Gold <= GoldCount &&
            Cost.Platinum <= PlatinumCount &&
            Cost.Obsidian <= ObsidianCount &&
            Cost.Diamond <= DiamondCount)
        {
            DirtCount -= Cost.Dirt;
            StoneCount -= Cost.Stone;
            TinCount -= Cost.Tin;
            CopperCount -= Cost.Copper;
            IronCount -= Cost.Iron;
            SilverCount -= Cost.Silver;
            GoldCount -= Cost.Gold;
            PlatinumCount -= Cost.Platinum;
            ObsidianCount -= Cost.Obsidian;
            DiamondCount -= Cost.Diamond;
            return true;
        }

        return false;
    }

    public void AddUnitInventory(UnitInventory unitInventory)
    {
        foreach (var i in unitInventory.Inventory)
        {
            if(i.Value <= 0)
                continue;

            switch (i.Key)
            {
                case UnitInventory.InventoryItems.Dirt:
                    DirtCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Stone:
                    StoneCount+=i.Value;
                    break;
                case UnitInventory.InventoryItems.Tin:
                    TinCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Copper:
                    CopperCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Iron:
                    IronCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Silver:
                    SilverCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Gold:
                    GoldCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Platinum:
                    PlatinumCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Obsidian:
                    ObsidianCount += i.Value;
                    break;
                case UnitInventory.InventoryItems.Diamond:
                    DiamondCount += i.Value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
