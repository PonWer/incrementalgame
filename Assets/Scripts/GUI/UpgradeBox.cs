﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Unit;
using Assets.Scripts.Unit.Upgrades;
using Manager;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpgradeBox : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public UpgradeList ParentList;
    public Image _Image;
    public Text MiddleText;
    public Text BuyButtonText;
    public Text PopUpText;
    public GameObject PopUpGameObject;

    public ArmorUpgrade _ArmorUpgrade = null;
    public InventoryUpgrade _InventoryUpgrade = null;
    public SwordUpgrade _SwordUpgrade = null;
    public PickaxeUpgrade _PickaxeUpgrade = null;

    public void BuyButtonPressed()
    {
        if (_ArmorUpgrade != null)
        {
            if (InventoryManager.GetInstance().CanSpend(_ArmorUpgrade.Cost))
            {
                if(UnitManager.GetInstance().CurrentUpgrades.UpgradeArmor())
                    ParentList.AddNextArmor();
                Destroy(gameObject);
            }
        }

        if (_InventoryUpgrade != null)
        {
            if (InventoryManager.GetInstance().CanSpend(_InventoryUpgrade.Cost))
            {
                if (UnitManager.GetInstance().CurrentUpgrades.UpgradeInventory())
                    ParentList.AddNextInventory();
                Destroy(gameObject);
            }
        }

        if (_SwordUpgrade != null)
        {
            if (InventoryManager.GetInstance().CanSpend(_SwordUpgrade.Cost))
            {
                if (UnitManager.GetInstance().CurrentUpgrades.UpgradeSword())
                    ParentList.AddNextSword();
                Destroy(gameObject);
            }
        }

        if (_PickaxeUpgrade != null)
        {
            if (InventoryManager.GetInstance().CanSpend(_PickaxeUpgrade.Cost))
            {
                if (UnitManager.GetInstance().CurrentUpgrades.UpgradePickaxe())
                    ParentList.AddNextPickaxe();
                Destroy(gameObject);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        PopUpGameObject.SetActive(true);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PopUpGameObject.SetActive(false);
    }
}
