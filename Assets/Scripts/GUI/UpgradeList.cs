﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Manager.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeList : MonoBehaviour
{
    public Dropdown FilterDropDown;
    public List<UpgradeBox> UpgradeBoxes = new List<UpgradeBox>();


    // Start is called before the first frame update
    void Start()
    {
        FilterDropDown.onValueChanged.AddListener(delegate { OnDropDownChanged();});

        AddNextArmor();
        AddNextInventory();
        AddNextPickaxe();
        AddNextSword();
    }

    public bool AppendCostToDescription = false;

    void OnDropDownChanged()
    {
        
    }

    UpgradeBox SpawnNew()
    {
        var newUpgrade = Instantiate(CustomAssetBundle.GetInstance().GetPrefab("upgradebox"), Vector3.zero,
            Quaternion.identity);
        newUpgrade.transform.SetParent(this.transform);
        var returnthing = newUpgrade.GetComponent<UpgradeBox>();
        UpgradeBoxes.Add(returnthing);
        returnthing.ParentList = this;
        return returnthing;
    }

    public void AddNextArmor()
    {
        var box = SpawnNew();
        var upgrade = UnitManager.GetInstance().CurrentUpgrades.Armor[UnitManager.GetInstance().CurrentUpgrades.iArmor + 1];
        box.MiddleText.text = upgrade.Name;
        box.PopUpText.text = upgrade.UpgradeInfo + (AppendCostToDescription ? $"\r\n{upgrade.Cost.GetContent()}": "");
        box._Image.sprite = CustomAssetBundle.GetInstance().GetSprite(upgrade.Image);
        box._ArmorUpgrade = upgrade;

        //box._ArmorUpgrade = null;
        box._InventoryUpgrade = null;
        box._SwordUpgrade = null;
        box._PickaxeUpgrade = null;
    }

    public void AddNextInventory()
    {
        var box = SpawnNew();
        var upgrade = UnitManager.GetInstance().CurrentUpgrades.Inventory[UnitManager.GetInstance().CurrentUpgrades.iInventory + 1];
        box.MiddleText.text = upgrade.Name;
        box.PopUpText.text = upgrade.UpgradeInfo + (AppendCostToDescription ? $"\r\n{upgrade.Cost.GetContent()}" : "");
        box._Image.sprite = CustomAssetBundle.GetInstance().GetSprite(upgrade.Image);
        box._InventoryUpgrade = upgrade;

        box._ArmorUpgrade = null;
        //box._InventoryUpgrade = null;
        box._SwordUpgrade = null;
        box._PickaxeUpgrade = null;

    }

    public void AddNextSword()
    {
        var box = SpawnNew();
        var upgrade = UnitManager.GetInstance().CurrentUpgrades.Sword[UnitManager.GetInstance().CurrentUpgrades.iSword + 1];
        box.MiddleText.text = upgrade.Name;
        box.PopUpText.text = upgrade.UpgradeInfo + (AppendCostToDescription ? $"\r\n{upgrade.Cost.GetContent()}" : "");
        box._Image.sprite = CustomAssetBundle.GetInstance().GetSprite(upgrade.Image);
        box._SwordUpgrade = upgrade;

        box._ArmorUpgrade = null;
        box._InventoryUpgrade = null;
        //box._SwordUpgrade = null;
        box._PickaxeUpgrade = null;
    }

    public void AddNextPickaxe()
    {
        var box = SpawnNew();
        var upgrade = UnitManager.GetInstance().CurrentUpgrades.Pickaxe[UnitManager.GetInstance().CurrentUpgrades.iPickaxe + 1];
        box.MiddleText.text = upgrade.Name;
        box.PopUpText.text = upgrade.UpgradeInfo + (AppendCostToDescription ? $"\r\n{upgrade.Cost.GetContent()}" : "");
        box._Image.sprite = CustomAssetBundle.GetInstance().GetSprite(upgrade.Image);
        box._PickaxeUpgrade = upgrade;

        box._ArmorUpgrade = null;
        box._InventoryUpgrade = null;
        box._SwordUpgrade = null;
        //box._PickaxeUpgrade = null;
    }
}
