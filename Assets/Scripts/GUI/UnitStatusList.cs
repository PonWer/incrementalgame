﻿using System.Collections;
using System.Collections.Generic;
using Manager.Scripts.Tile;
using Manager.Scripts.Unit;
using Manager.Scripts.Utility;
using UnityEngine;

public class UnitStatusList : MonoBehaviour
{

    public void AddUnit(Unit unit)
    {
        var unitscript = Instantiate(CustomAssetBundle.GetInstance().GetPrefab("unitstatusbox"), Vector3.zero , Quaternion.identity).GetComponent<UnitScript>();
        unitscript.transform.SetParent(this.transform);
        unitscript.AssignedUnit = unit;
        unit.unitscript = unitscript;
        unitscript.UpdateText();
    }

    void Awake()
    {
        _instance = this;
    }

    private static UnitStatusList _instance;
    public static UnitStatusList GetInstance()
    {
        return _instance;
    }
}
