﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.UI;

public class InventoryScreen : MonoBehaviour
{
    public enum Buildings
    {
        Unit = 0,
        House
    }

    public Text UnitCountText = null;
    public Text HouseCountText = null;

    public Text CurrentEquipmentStatus = null;

    public Text DirtCountText = null;
    public Text StoneCountText = null;
    public Text TinCountText = null;
    public Text CopperCountText = null;
    public Text IronCountText = null;
    public Text SilverCountText = null;
    public Text GoldCountText = null;
    public Text PlatinumCountText = null;
    public Text ObsidianCount = null;
    public Text DiamondCountText = null;





    // Start is called before the first frame update
    void Start()
    {
        if (StoneCountText == null) Debug.LogError("Saknas ju för fan StoneCountText");
        if (DirtCountText == null) Debug.LogError("Saknas ju för fan DirtCountText");
        if (TinCountText == null) Debug.LogError("Saknas ju för fan TinCountText");
    }

    // Update is called once per frame
    void Update()
    {
        //todo update value only on value changed
        UnitCountText.text = UnitManager.GetInstance().GetUnitsStatus();
        HouseCountText.text = $"Barracts: {InventoryManager.GetInstance().HouseCount}";
        StoneCountText.text = $"Stones: {InventoryManager.GetInstance().StoneCount}";
        DirtCountText.text = $"Dirt: {InventoryManager.GetInstance().DirtCount}";
        TinCountText.text = $"Tin: {InventoryManager.GetInstance().TinCount}";
        CopperCountText.text = $"Copper: {InventoryManager.GetInstance().CopperCount}";
        IronCountText.text = $"Iron: {InventoryManager.GetInstance().IronCount}";
        SilverCountText.text = $"Silver: {InventoryManager.GetInstance().SilverCount}";
        GoldCountText.text = $"Gold: {InventoryManager.GetInstance().GoldCount}";
        PlatinumCountText.text = $"Platinum: {InventoryManager.GetInstance().PlatinumCount}";
        ObsidianCount.text = $"Obsidian: {InventoryManager.GetInstance().ObsidianCount}";
        DiamondCountText.text = $"Diamond: {InventoryManager.GetInstance().DiamondCount}";
        CurrentEquipmentStatus.text = UnitManager.GetInstance().GetEquipmentStatus();
    }
}
