﻿using Manager.Scripts.Tile;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Camera
{
    public class CameraMovement : MonoBehaviour
    {
        private UnityEngine.Camera _camera;

        public void OnZoomButton(bool In)
        {
            if (In)
            {
                // scroll up
                var cameraOrthographicSize = _camera.orthographicSize - ScrollSpeed;
                _camera.orthographicSize = cameraOrthographicSize < Min ? Min : cameraOrthographicSize;
            }
            else
            {
                // scroll down
                var cameraOrthographicSize = _camera.orthographicSize + ScrollSpeed;
                _camera.orthographicSize = cameraOrthographicSize > Max ? Max : cameraOrthographicSize;
            }
        }

        public void OnZoomButtonRestore()
        {
            _camera.orthographicSize = Max;
        }

        //private bool _leftCtrlPressed;
        public float Max = 30f;
        public float Min = 10f;

        public float ScrollSpeed = 1f;
        private Vector3 _startMousePos;

        private void Start()
        {
            _camera = GetComponent<UnityEngine.Camera>();
        }

        private void LateUpdate()
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return;

            ScrollWheel();

            ArrowsAndWASD();

            MouseButton();
        }


        private void ArrowsAndWASD()
        {
            var xAxisValue = Input.GetAxis("Horizontal");
            var yAxisValue = Input.GetAxis("Vertical");

            MoveCamera(new Vector3(transform.position.x + xAxisValue,
                transform.position.y + yAxisValue,
                transform.position.z));
        }

        private void ScrollWheel()
        {
            var d = Input.GetAxis("Mouse ScrollWheel");
            //Camera view size
            if (d > 0f)
            {
                if(_camera.orthographicSize ==  Min)
                    return;

                // scroll up
                var cameraOrthographicSize = _camera.orthographicSize - ScrollSpeed;
                _camera.orthographicSize = cameraOrthographicSize < Min ? Min : cameraOrthographicSize;

                var MousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
                MousePos.z = transform.position.z;

                transform.position = Vector3.MoveTowards(transform.position, MousePos, ScrollSpeed);
            }
            else if (d < 0f)
            {
                // scroll down
                var cameraOrthographicSize = _camera.orthographicSize + ScrollSpeed;
                _camera.orthographicSize = cameraOrthographicSize > Max ? Max : cameraOrthographicSize;
            }
            
        }

        private void MouseButton()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
            {
                _startMousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
                _startMousePos.z = 0.0f;
            }

            if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
            {
                var nowMousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
                nowMousePos.z = 0.0f;
                MoveCamera(transform.position + _startMousePos - nowMousePos);
            }
        }

        private void MoveCamera(Vector3 pos)
        {
            var bufferDistance = 3.5f;

            var minX = _camera.orthographicSize * _camera.aspect;
            var minY = _camera.orthographicSize;

            var maxX = TileManager.GetInstance().maxX -1 - _camera.orthographicSize * _camera.aspect;
            var maxY = TileManager.GetInstance().maxY -1 - _camera.orthographicSize;

            pos = new Vector3(
                Mathf.Clamp(pos.x, minX - bufferDistance*3.5f, maxX + bufferDistance),
                Mathf.Clamp(pos.y, minY - bufferDistance, maxY + bufferDistance),
                pos.z);

            transform.position = pos;
        }
    }
}