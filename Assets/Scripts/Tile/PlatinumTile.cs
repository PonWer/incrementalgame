﻿using System;
using System.Collections.Generic;
using Manager.Scripts.Utility;
using UnityEngine;

namespace Manager.Scripts.Tile
{
    public class PlatinumTile : BaseTile
    {
        public override void Start()
        {
            base.Start();
            RemainingMiningHealth = TileManager.GetInstance().PlatinumHealth;
        }

        public override TileType GetTileType() => TileType.Platinum;

        public override Sprite GetSprite()
        {
            var spritName = "Platinum" + UnityEngine.Random.Range(1, 3).ToString();
            return CustomAssetBundle.GetInstance().GetSprite(spritName);
        }
        public override ColliderType GetColliderType() => UnityEngine.Tilemaps.Tile.ColliderType.Grid;

        public override void AddToInventory(UnitInventory inv)
        {
            inv.ChangeAmount(UnitInventory.InventoryItems.Platinum, 1);
        }
    }
}