﻿using System;
using Manager.Scripts.Utility;
using UnityEngine;

namespace Manager.Scripts.Tile
{
    public class GroundTile : BaseTile
    {
        public override TileType GetTileType() => TileType.Ground;

        public override Sprite GetSprite()
        {
            var spritName = "ground" + UnityEngine.Random.Range(1, 4).ToString();
            return CustomAssetBundle.GetInstance().GetSprite(spritName);
        }
        public override ColliderType GetColliderType() => UnityEngine.Tilemaps.Tile.ColliderType.None;

        public override void AddToInventory(UnitInventory inv)
        {

        }
    }
}