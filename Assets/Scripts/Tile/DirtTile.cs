﻿using System;
using System.Collections.Generic;
using Manager.Scripts.Utility;
using UnityEngine;
using Random = System.Random;

namespace Manager.Scripts.Tile
{
    public class DirtTile : BaseTile
    {
        public override void Start()
        {
            base.Start();
            RemainingMiningHealth = TileManager.GetInstance().DirtHealth;
        }

        public override TileType GetTileType() => TileType.Dirt;
        public override Sprite GetSprite()
        {
            var spritName = "dirt" + UnityEngine.Random.Range(1, 3).ToString();
            return CustomAssetBundle.GetInstance().GetSprite(spritName);
        }

        public override ColliderType GetColliderType() => UnityEngine.Tilemaps.Tile.ColliderType.Grid;

        public override void AddToInventory(UnitInventory inv)
        {
            inv.ChangeAmount(UnitInventory.InventoryItems.Dirt,1);
        }
    }
}