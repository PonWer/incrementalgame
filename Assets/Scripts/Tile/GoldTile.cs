﻿using System;
using System.Collections.Generic;
using Manager.Scripts.Utility;
using UnityEngine;

namespace Manager.Scripts.Tile
{
    public class GoldTile : BaseTile
    {
        public override void Start()
        {
            base.Start();
            RemainingMiningHealth = TileManager.GetInstance().GoldHealth;
        }

        public override TileType GetTileType() => TileType.Gold;

        public override Sprite GetSprite()
        {
            var spritName = "Gold" + UnityEngine.Random.Range(1, 3).ToString();
            return CustomAssetBundle.GetInstance().GetSprite(spritName);
        }
        public override ColliderType GetColliderType() => UnityEngine.Tilemaps.Tile.ColliderType.Grid;

        public override void AddToInventory(UnitInventory inv)
        {
            inv.ChangeAmount(UnitInventory.InventoryItems.Gold, 1);
        }
    }
}