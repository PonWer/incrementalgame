﻿using System;
using System.Collections.Generic;
using Manager.Scripts.Utility;
using UnityEngine;

namespace Manager.Scripts.Tile
{
    public class ObsidianTile : BaseTile
    {
        public override void Start()
        {
            base.Start();
            RemainingMiningHealth = TileManager.GetInstance().ObsidianHealth;
        }

        public override TileType GetTileType() => TileType.Obsidian;

        public override Sprite GetSprite()
        {
            var spritName = "Obsidian" + UnityEngine.Random.Range(1, 3).ToString();
            return CustomAssetBundle.GetInstance().GetSprite(spritName);
        }
        public override ColliderType GetColliderType() => UnityEngine.Tilemaps.Tile.ColliderType.Grid;

        public override void AddToInventory(UnitInventory inv)
        {
            inv.ChangeAmount(UnitInventory.InventoryItems.Obsidian, 1);
        }
    }
}