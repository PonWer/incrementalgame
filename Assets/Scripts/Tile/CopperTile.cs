﻿using System;
using System.Collections.Generic;
using Manager.Scripts.Utility;
using UnityEngine;

namespace Manager.Scripts.Tile
{
    public class CopperTile : BaseTile
    {
        public override void Start()
        {
            base.Start();
            RemainingMiningHealth = TileManager.GetInstance().CopperHealth;
        }

        public override TileType GetTileType() => TileType.Copper;

        public override Sprite GetSprite()
        {
            var spritName = "copper" + UnityEngine.Random.Range(1, 3).ToString();
            return CustomAssetBundle.GetInstance().GetSprite(spritName);
        }
        public override ColliderType GetColliderType() => UnityEngine.Tilemaps.Tile.ColliderType.Grid;

        public override void AddToInventory(UnitInventory inv)
        {
            inv.ChangeAmount(UnitInventory.InventoryItems.Copper, 1);
        }
    }
}