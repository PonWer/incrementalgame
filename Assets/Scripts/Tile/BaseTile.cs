﻿using System;
using Manager.Scripts.Utility;
using UnityEngine;

namespace Manager.Scripts.Tile
{
    public abstract class BaseTile : UnityEngine.Tilemaps.Tile
    {
        public enum TileType
        {
            Ground,
            Rock,
            Dirt,
            Tin,
            Copper,
            Iron,
            Silver,
            Gold,
            Platinum,
            Obsidian,
            Diamond
        }

        public float RemainingMiningHealth;

        public Vector3Int Coordinate { get; set; }

        public virtual void Start()
        {
            TileManager.GetInstance().SetColor(Coordinate, Hidden ? HiddenColor() : Color.white);
        }
        public abstract void AddToInventory(UnitInventory inv);

        public BaseTile ReplaceWithGroundTile()
        {
            var newGround = CreateInstance<GroundTile>();
            TileManager.GetInstance().ReplaceTile(this, newGround);
            TileManager.GetInstance().AddNeighborsAsAvailable(Coordinate);
            return newGround;
        }

        public virtual Color NeutralColor()
        {
            return Color.white;
        }
        public virtual Color HiddenColor()
        {
            return Color.black;
        }

        public abstract TileType GetTileType();
        public abstract Sprite GetSprite();

        public abstract ColliderType GetColliderType();
        public bool Hidden = false;
    }
}