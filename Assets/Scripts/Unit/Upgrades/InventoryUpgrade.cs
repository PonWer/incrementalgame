﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Unit.Upgrades
{
    [System.Serializable]
    public class InventoryUpgrade
    {
        public string Name = "Pointy Stick";
        public int Slots = 10;
        [TextArea(2, 10)]
        public string UpgradeInfo = "Increases bla to bla";
        public string Image = "sword1";
        public UnitUpgrades.UpgradeCost Cost = new UnitUpgrades.UpgradeCost();
    }
}
