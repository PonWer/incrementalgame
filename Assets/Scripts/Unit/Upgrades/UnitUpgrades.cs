﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Unit.Upgrades;
using Manager;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    [System.Serializable]
    public class UnitUpgrades
    {
        public ArmorUpgrade[] Armor;
        public InventoryUpgrade[] Inventory;
        public PickaxeUpgrade[] Pickaxe;
        public SwordUpgrade[] Sword;

        public int iArmor = 0;
        public int iInventory = 0;
        public int iPickaxe = 0;
        public int iSword = 0;

        public string ArmorName => Armor[iArmor].Name;
        public float ArmorHealth => Armor[iArmor].Health;
        public UpgradeCost ArmorCost => Armor[iArmor].Cost;
        public string ArmorUpgradeInfo => Armor[iArmor].UpgradeInfo;
        public string ArmorImage => Armor[iArmor].Image;

        public string InventoryName => Inventory[iInventory].Name;
        public int InventorySlots => Inventory[iInventory].Slots;
        public UpgradeCost InventoryCost => Inventory[iInventory].Cost;
        public string InventoryUpgradeInfo => Inventory[iInventory].UpgradeInfo;
        public string InventoryImage => Inventory[iInventory].Image;

        public string PickaxeName => Pickaxe[iPickaxe].Name;
        public float PickaxeDamage => Pickaxe[iPickaxe].Damage;
        public float PickaxeSpeed => Pickaxe[iPickaxe].Speed;
        public UpgradeCost PickaxeCost => Pickaxe[iPickaxe].Cost;
        public string PickaxeUpgradeInfo => Pickaxe[iPickaxe].UpgradeInfo;
        public string PickaxeImage => Pickaxe[iPickaxe].Image;

        public string SwordName => Sword[iSword].Name;
        public float SwordDamage => Sword[iSword].Damage;
        public float SwordSpeed => Sword[iSword].Speed;
        public UpgradeCost SwordCost => Sword[iSword].Cost;
        public string SwordUpgradeInfo => Sword[iSword].UpgradeInfo;
        public string SwordImage => Sword[iSword].Image;

        public UnitUpgrades(TextAsset text)
        {
            Armor = new ArmorUpgrade[3];
            Inventory = new InventoryUpgrade[3];
            Pickaxe = new PickaxeUpgrade[3];
            Sword = new SwordUpgrade[3];

            for (int i = 0; i < 3; i++) Armor[i] = new ArmorUpgrade();
            for (int i = 0; i < 3; i++) Inventory[i] = new InventoryUpgrade();
            for (int i = 0; i < 3; i++) Pickaxe[i] = new PickaxeUpgrade();
            for (int i = 0; i < 3; i++) Sword[i] = new SwordUpgrade();
        }

        public bool UpgradeArmor()
        {
            iArmor++;
            return iArmor < Armor.Length - 1;
        }

        public bool UpgradeSword()
        {
            iSword++;
            return iSword < Sword.Length - 1;
        }

        public bool UpgradeInventory()
        {
            iInventory++;
            return iInventory < Inventory.Length - 1;
        }

        public bool UpgradePickaxe()
        {
            iPickaxe++;
            return iPickaxe < Pickaxe.Length - 1;
        }

        public static UnitUpgrades CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<UnitUpgrades>(jsonString);
        }

        public void WriteJSON()
        {
            var fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "IdleGame\\");
            System.IO.Directory.CreateDirectory(fileName);
            fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "IdleGame\\Upgrades.txt");
            Debug.Log(fileName);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false))
            {
                file.WriteLine(GetJSON());
            }
        }

        public string GetJSON()
        {
            return JsonUtility.ToJson(this,true);
        }

        [System.Serializable]
        public struct UpgradeCost
        {
            public int Dirt;
            public int Stone;
            public int Tin;
            public int Copper;
            public int Iron;
            public int Silver;
            public int Gold;
            public int Platinum;
            public int Obsidian;
            public int Diamond;


            public string GetContent()
            {
                string returnText = "\nCost:\n";
                if (Dirt > 0) returnText += $"\tDirt: {Dirt}\n";
                if (Stone > 0) returnText += $"\tStone: {Stone}\n";
                if (Tin > 0) returnText += $"\tTin: {Tin}\n";
                if (Copper > 0) returnText += $"\tCopper: {Copper}\n";
                if (Iron > 0) returnText += $"\tIron: {Iron}\n";
                if (Silver > 0) returnText += $"\tSilver: {Silver}\n";
                if (Gold > 0) returnText += $"\tGold: {Gold}\n";
                if (Platinum > 0) returnText += $"\tPlatinum: {Platinum}\n";
                if (Obsidian > 0) returnText += $"\tObsidian: {Obsidian}\n";
                if (Diamond > 0) returnText += $"\tDiamond: {Diamond}\n";
                return returnText;
            }
        }
    }
}