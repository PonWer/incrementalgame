﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Unit.Upgrades
{
    [System.Serializable]
    public class ArmorUpgrade
    {
        public string Name = "Pointy Stick";
        public int Health = 3;
        [TextArea(2, 10)]
        public string UpgradeInfo = "Increases bla to bla";
        public string Image = "sword1";
        public UnitUpgrades.UpgradeCost Cost = new UnitUpgrades.UpgradeCost();
    }
}
