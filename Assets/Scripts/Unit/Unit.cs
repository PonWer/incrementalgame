﻿using System;
using System.Collections.Generic;
using System.Linq;
using Manager.Scripts.Tile;
using Manager.Scripts.Unit.State;
using Pathfinding;
using UnityEngine;

namespace Manager.Scripts.Unit
{
    public class Unit : MonoBehaviour
    {
        public BaseState CurrentState { get; private set; } = IdleState.Instance;
        public TextMesh IdLabel;
        public UnitInventory Inventory = new UnitInventory();
        private static int currentUnitcount = 0;
        public float RemainingHealth { get; internal set; } = 20;
        public IAstarAI ai;
        public UnitScript unitscript;

        public BaseTile TargetBaseTile = null;
        public Enemy TargetEnemy = null;

        public float MaxStartCombatDistance = 10f;

        void OnEnable()
        {
            ai = GetComponent<IAstarAI>();
            ai.onSearchPath += UpdatePath;
        }

        internal float GetDistanceToDestination()
        {
            if (TargetBaseTile != null)
                return Vector3.Distance(transform.position,TargetBaseTile.Coordinate);
            else if (TargetEnemy != null)
                return Vector3.Distance(transform.position, TargetEnemy.transform.position);
            return 0;
        }

        void OnDisable()
        {
            ai.onSearchPath -= UpdatePath;
            if (TargetBaseTile != null)
                TileManager.GetInstance().ReturnTile(TargetBaseTile);

            if(unitscript == null) return;

            unitscript.AssignedUnit = null;
            Destroy(unitscript.gameObject);
            unitscript = null;
        }

        public void UpdatePath()
        {
            if (TargetBaseTile != null)
            {
                //if (TargetBaseTile.Walkable)
                    ai.destination = TargetBaseTile.Coordinate;
                //else
                   // ai.destination = TileManager.GetInstance()._AstarPath.GetNearest(TargetBaseTile.Coordinate).position;
            }           
            else if(TargetEnemy != null)
                ai.destination = TargetEnemy.transform.position;

        }

        private void Start()
        {
            CurrentState.OnStateEnter(this);
            IdLabel.text = (currentUnitcount++).ToString();
            gameObject.name = IdLabel.text;
            RemainingHealth = UnitManager.GetInstance().CurrentUpgrades.ArmorHealth;
            UnitStatusList.GetInstance().AddUnit(this);
            unitscript.UpdateText();
        }

        public void FixedUpdate()
        {
            CurrentState.OnStateUpdate(this);
        }

        public void ChangeState(BaseState nextState)
        {
            if (CurrentState == nextState)
                return;

            CurrentState.OnStateExit(this);
            CurrentState = nextState;
            CurrentState.OnStateEnter(this);
        }

        public void Clean()
        {
            TargetEnemy = null;
            TargetBaseTile = null;
        }

        public void Death()
        {
            LogManager.GetInstance().AddLog($"Unit {name} has been killed");
            UnitManager.GetInstance()._Units.Remove(this);
            Destroy(gameObject);
        }

        public string GetHeathStatus()
        {
            return $"{RemainingHealth}/{UnitManager.GetInstance().CurrentUpgrades.ArmorHealth}";
        }

        public void TakeDamage(float damage)
        {
            RemainingHealth -= damage;
            unitscript.UpdateText();
            if (RemainingHealth < 0)
                Death();

        }
    }
}