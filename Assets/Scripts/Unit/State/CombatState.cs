﻿using Manager.Scripts.Tile;
using UnityEngine;

namespace Manager.Scripts.Unit.State
{
    public class CombatState : BaseState
    {
        private static CombatState _instance;
        public static CombatState Instance => _instance ?? (_instance = new CombatState());

        public override void OnStateEnter(Unit unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=blue>Mining State</color>");
        }

        public override void OnStateExit(Unit unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=blue>Mining State</color>");
            unit.Clean();
        }

        public override void OnStateUpdate(Unit unit)
        {
            if (unit.TargetEnemy.IsDead)
            {
                unit.ChangeState(IdleState.Instance);
                return;
            }

            unit.UpdatePath();
            if (Vector3.Distance(unit.transform.position, unit.TargetEnemy.transform.position) < 1f)
            {
                if (unit.TargetEnemy.RemainingHealth < 0)
                {
                    unit.TargetEnemy.Death(unit.name);
                    unit.ChangeState(IdleState.Instance);
                    return;
                }
                else
                {
                    unit.TargetEnemy.RemainingHealth -= Time.deltaTime *
                                         (UnitManager.GetInstance().CurrentUpgrades.SwordDamage /
                                          UnitManager.GetInstance().CurrentUpgrades.SwordSpeed);
                }
            }
        }
    }
}