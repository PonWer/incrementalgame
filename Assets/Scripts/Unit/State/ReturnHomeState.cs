﻿using System;
using Manager.Scripts.Tile;
using UnityEngine;

namespace Manager.Scripts.Unit.State
{
    public class ReturnHomeState : BaseState
    {
        private static ReturnHomeState _instance;
        public static ReturnHomeState Instance => _instance ?? (_instance = new ReturnHomeState());

        public override void OnStateEnter(Unit unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=cyan>ReturnHomeState State</color>");
            unit.TargetBaseTile = TileManager.GetInstance().GetTile(TileManager.GetInstance().HomePos);
            unit.UpdatePath();
        }

        public override void OnStateExit(Unit unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=cyan>ReturnHomeState State</color>");
            unit.Clean();
        }


        public override void OnStateUpdate(Unit unit)
        {
            //unit.SetDestination(TileManager.GetInstance().HomePos);
            if (unit.GetDistanceToDestination() < 1f)
            {
                InventoryManager.GetInstance().AddUnitInventory(unit.Inventory);
                unit.Inventory.Reset();
                unit.unitscript.UpdateText();
                unit.ChangeState(IdleState.Instance);
            }
        }
    }
}