﻿using System.IO;
using Manager.Scripts.Tile;
using UnityEngine;

namespace Manager.Scripts.Unit.State
{
    public class MiningState : BaseState
    {
        private static MiningState _instance;
        public static MiningState Instance => _instance ?? (_instance = new MiningState());

        public override void OnStateEnter(Unit unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=blue>Mining State</color>");
            unit.UpdatePath();
        }

        public override void OnStateExit(Unit unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=blue>Mining State</color>");
            unit.Clean();
        }

        public override void OnStateUpdate(Unit unit)
        {
            if (unit.GetDistanceToDestination() > 1f)
                return;

            if (unit.TargetBaseTile == null)
            {
                unit.ChangeState(IdleState.Instance);
                return;
            }

            if (unit.TargetBaseTile.RemainingMiningHealth < 0)
            {
                var target = unit.TargetBaseTile;
                unit.Clean();

                if(target.GetTileType() == BaseTile.TileType.Ground)
                    throw new InvalidDataException("ground as target");

                target.AddToInventory(unit.Inventory);
                target.ReplaceWithGroundTile();
                unit.unitscript.UpdateText();
                unit.ChangeState(IdleState.Instance);
            }
            else
                unit.TargetBaseTile.RemainingMiningHealth -= Time.deltaTime *
                    (UnitManager.GetInstance().CurrentUpgrades.PickaxeDamage /
                     UnitManager.GetInstance().CurrentUpgrades.PickaxeSpeed);
        }
    }
}