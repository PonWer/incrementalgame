﻿using System;
using System.Linq;
using Manager.Scripts.Tile;
using UnityEngine;

namespace Manager.Scripts.Unit.State
{
    public class IdleState : BaseState
    {
        private static IdleState _instance;
        public static IdleState Instance => _instance ?? (_instance = new IdleState());

        public override void OnStateEnter(Unit unit)
        {
            if (logEnteringStates) Debug.Log("Entering: <color=cyan>Idle State</color>");
            
        }

        public override void OnStateExit(Unit unit)
        {
            if (logLeavingStates) Debug.Log("Leaving: <color=cyan>Idle State</color>");
        }


        public override void OnStateUpdate(Unit unit)
        {
            unit.TargetEnemy = EnemyManager.GetInstance().GetClosestAwakeEnemy(unit.transform.position, unit.MaxStartCombatDistance);
            if (unit.TargetEnemy != null)
            {
                unit.ChangeState(CombatState.Instance);
            }
            else if (unit.Inventory.IsFull())
            {
                unit.ChangeState(ReturnHomeState.Instance);
            }
            else
            {
                unit.TargetBaseTile = TileManager.GetInstance().GetClosestAvailableTile2(unit.transform.position);
                if (unit.TargetBaseTile != null)
                {
                    unit.ChangeState(MiningState.Instance);
                }
                else
                {
                    unit.TargetBaseTile = TileManager.GetInstance().GetTile(TileManager.GetInstance().HomePos);
                }
            }
        }
    }
}