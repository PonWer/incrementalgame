﻿namespace Manager.Scripts.Unit.State
{
    public abstract class BaseState
    {
        public bool logEnteringStates = false;
        public bool logLeavingStates = false;

        public abstract void OnStateEnter(Unit unit);

        public abstract void OnStateExit(Unit unit);

        public abstract void OnStateUpdate(Unit unit);
    }
}