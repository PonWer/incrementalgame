﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class UnitInventory
{
    public UnitInventory()
    {
        Inventory.Add(InventoryItems.Dirt, 0);
        Inventory.Add(InventoryItems.Stone, 0);
        Inventory.Add(InventoryItems.Tin, 0);
        Inventory.Add(InventoryItems.Copper, 0);
        Inventory.Add(InventoryItems.Iron, 0);
        Inventory.Add(InventoryItems.Silver, 0);
        Inventory.Add(InventoryItems.Gold, 0);
        Inventory.Add(InventoryItems.Platinum, 0);
        Inventory.Add(InventoryItems.Obsidian, 0);
        Inventory.Add(InventoryItems.Diamond, 0);
    }

    public enum InventoryItems
    {
        Dirt,
        Stone,
        Tin,
        Copper,
        Iron,
        Silver,
        Gold,
        Platinum,
        Obsidian,
        Diamond
    }

    public void Reset()
    {
        count = 0;
        Inventory[InventoryItems.Dirt] = 0;
        Inventory[InventoryItems.Stone] = 0;
        Inventory[InventoryItems.Tin] = 0;
        Inventory[InventoryItems.Copper] = 0;
        Inventory[InventoryItems.Iron] = 0;
        Inventory[InventoryItems.Silver] = 0;
        Inventory[InventoryItems.Gold] = 0;
        Inventory[InventoryItems.Platinum] = 0;
        Inventory[InventoryItems.Obsidian] = 0;
        Inventory[InventoryItems.Diamond] = 0;
    }

    public readonly Dictionary<InventoryItems,int> Inventory = new Dictionary<InventoryItems, int>();

    public void ChangeAmount(InventoryItems type, int amount)
    {
        Inventory[type] += amount;
        count += amount;
    }

    private int count = 0;
    public bool IsFull()
    {
        return count >= UnitManager.GetInstance().CurrentUpgrades.InventorySlots;
    }

    public string GetStatus()
    {
        return $"{count}/{UnitManager.GetInstance().CurrentUpgrades.InventorySlots}";
    }
}
