﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Manager.Scripts.Tile;
using Manager.Scripts.Utility;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public List<Enemy> _Enemies = new List<Enemy>();
    public List<Enemy> _SleepingEnemies = new List<Enemy>();
    public List<Enemy> _NotSoSleepingEnemies = new List<Enemy>();

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {


    }

    public Enemy DirectlySpawnEnemy(Vector3 pos)
    {
        var unit = Instantiate(CustomAssetBundle.GetInstance().GetPrefab("enemy"), pos + new Vector3(0.5f,0.5f) , Quaternion.identity).GetComponent<Enemy>();
        unit.gameObject.transform.SetParent(gameObject.transform, true);
        _Enemies.Add(unit);
        _SleepingEnemies.Add(unit);
        return unit;
    }

    private static EnemyManager _instance;
    public static EnemyManager GetInstance()
    {
        return _instance;
    }

    public void EnableMonstersAtCoordinate(Vector3Int tileCoordinate)
    {
        foreach (var enemy in _SleepingEnemies)
        {
            if (enemy.SleepingCoordinate != tileCoordinate)
                continue;

            enemy.gameObject.SetActive(true);
            enemy.Enabled = true;
            _NotSoSleepingEnemies.Add(enemy);
            _SleepingEnemies.Remove(enemy);
            return;
        }
    }

    public Enemy GetClosestAwakeEnemy(Vector3 pos, float MaxDistance)
    {
        Enemy CurrentClosest = null;
        float currentDistance = MaxDistance;

        foreach (var unit in _NotSoSleepingEnemies)
        {
            float distance = Math.Abs(pos.x - unit.transform.position.x) +
                             Math.Abs(pos.y - unit.transform.position.y);

            if (distance < currentDistance)
            {
                currentDistance = distance;
                CurrentClosest = unit;
            }
        }

        return CurrentClosest;
    }

    public void Reset()
    {

        while (_Enemies.Any())
        {
            Destroy(_Enemies.First().gameObject);
            _Enemies.RemoveAt(0);
        }

        _Enemies.Clear();
        _NotSoSleepingEnemies.Clear();
        _SleepingEnemies.Clear();
    }

}
