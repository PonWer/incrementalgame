﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Manager;
using Manager.Scripts.Tile;
using Manager.Scripts.Unit;
using Manager.Scripts.Unit.State;
using Pathfinding;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public TextMesh IdLabel;
    private static int currentEnemyCount = 0;
    public bool Enabled = false;
    public Unit TargetUnit = null;
    public Vector3Int SleepingCoordinate;
    public IAstarAI ai;

    private void Start()
    {
        IdLabel.text = (currentEnemyCount++).ToString();
        gameObject.name = IdLabel.text;
        ai = GetComponent<IAstarAI>();
    }

    public void FixedUpdate()
    {
        if (Enabled && TargetUnit == null)
        {
            TargetUnit = UnitManager.GetInstance().GetClosestUnitTo(transform.position);
        }

        if (TargetUnit)
        {
            ai.destination = TargetUnit.transform.position;

            if (Vector3.Distance(transform.position, TargetUnit.transform.position) < 1f)
            {
                TargetUnit.TakeDamage(Damage * Time.deltaTime);
            }
        }
    }

    public float Damage = 1;

    private BaseTile _destination;
    public List<BaseTile> Path { get; private set; } = new List<BaseTile>();
    public float RemainingHealth = 20;
    public bool IsDead = false;


    public void Death(string killer)
    {
        IsDead = true;
        EnemyManager.GetInstance()._Enemies.Remove(this);
        EnemyManager.GetInstance()._NotSoSleepingEnemies.Remove(this);
        LogManager.GetInstance().AddLog($"Enemy {name} was killed by {killer}");
        Destroy(gameObject);
        
    }
}
