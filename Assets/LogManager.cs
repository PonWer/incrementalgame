﻿using System.Collections;
using System.Collections.Generic;
using Manager.Scripts.Utility;
using UnityEngine;
using UnityEngine.UI;

public class LogManager : MonoBehaviour
{
    public Scrollbar Scrollbar;
    public void AddLog(string logMessage)
    {
        var unitscript = Instantiate(CustomAssetBundle.GetInstance().GetPrefab("logobject"), Vector3.zero, Quaternion.identity).GetComponent<LogObject>();
        unitscript.transform.SetParent(this.transform);
        unitscript.SetText(logMessage);
        Scrollbar.value = 0;
    }

    void Awake()
    {
        _instance = this;
    }

    private static LogManager _instance;
    public static LogManager GetInstance()
    {
        return _instance;
    }
}
