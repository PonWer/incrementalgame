﻿using System.Collections;
using System.Collections.Generic;
using Manager.Scripts.Unit;
using UnityEngine;
using UnityEngine.UI;

public class UnitScript : MonoBehaviour
{
    public Unit AssignedUnit;
    public Text Name;
    public Text Inventory;
    public Text Health;

    public void UpdateText()
    {
        if(!AssignedUnit)
            return;

        Name.text = AssignedUnit.name;
        Inventory.text = AssignedUnit.Inventory.GetStatus();
        Health.text = AssignedUnit.GetHeathStatus();
    }
}
