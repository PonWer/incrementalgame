﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogObject : MonoBehaviour
{
    public float TimeLife;
    public float MaxLifeTime = 20;
    public Text _Text;
    // Start is called before the first frame update
    public void SetText(string text)
    {
        //_Text = GetComponent<Text>();
        _Text.text = text;
    }

    void Update()
    {
        TimeLife += Time.deltaTime;
        if(TimeLife > MaxLifeTime)
            Destroy(gameObject);
    }
}
